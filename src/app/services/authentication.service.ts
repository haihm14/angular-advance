import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { UserManager, User } from 'oidc-client';
import { getClientSettings } from '../login/getClientSettings';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private manager = new UserManager(getClientSettings());
    private user: User | null;
    private _authNavStatusSource = new BehaviorSubject<boolean>(false);
    authNavStatus$ = this._authNavStatusSource.asObservable();
    token = '';

    constructor(private http: HttpClient) {
        this.manager.getUser().then(user => {
            this.user = user;
            debugger;
            localStorage.setItem('user', JSON.stringify(user));
            this._authNavStatusSource.next(this.isAuthenticated());
        });
    }

    startAuthentication(): Promise<void> {
        return this.manager.signinRedirect();
    }

    async completeAuthentication(): Promise<void> {
        return this.manager.signinRedirectCallback().then(user => {
            this.user = user;
            localStorage.setItem('user', JSON.stringify(user));
        });
    }

    isAuthenticated(): boolean {
        return this.user != null && !this.user.expired;
    }

    public getAccessToken(): string {
        const user = JSON.parse(localStorage.getItem('user'));
        console.log(this.user);
        return user?.access_token !=null ? user?.access_token : '' ;
    }

    get authorizationHeaderValue(): string {
        return `${this.user.token_type} ${this.user.access_token}`;
    }

    get name(): string {
        return this.user != null ? this.user.profile.name : '';
    }

    get roles(): string {
        return this.user != null ? this.user.profile.roles : '';
    }

    async signout() {
        await this.manager.signoutRedirect();
    }

    public getRequestHeaders(): { headers: HttpHeaders | { [header: string]: string | string[]; } } {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + this.user.access_token,
            'Content-Type': 'application/json',
        });
        return { headers };
    }

    public getUser() {
        this.manager.getUser().then(user => {
            this.user = user;
            localStorage.setItem('user', JSON.stringify(user));
            this._authNavStatusSource.next(this.isAuthenticated());
        });
    }
}