import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericResponse } from '../models/genericResponse';
import { LogLeave } from '../models/Leave';

@Injectable({
  providedIn: 'root'
})
export class LogLeaveService {

  constructor(private http: HttpClient) { }

  getAllLogLeave(){
    return this.http.get<GenericResponse>(`${environment.apiUrl}/api/LogLeave/GETAll`);
  }
  
  createLogLeave(data: LogLeave){
    return this.http.post<LogLeave>(`${environment.apiUrl}/api/LogLeave/Create`, data);
  }

  updateLogLeave(updateData: LogLeave ){
    return this.http.put<LogLeave>(`${environment.apiUrl}/api/LogLeave/Update`, updateData);
  }

  deleteLogLeave(id: string){
    return this.http.delete(`${environment.apiUrl}/api/LogLeave/Delete` + id);
  }

}
