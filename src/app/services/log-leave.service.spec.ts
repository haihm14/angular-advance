import { TestBed } from '@angular/core/testing';

import { LogLeaveService } from './log-leave.service';

describe('LogLeaveService', () => {
  let service: LogLeaveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogLeaveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
