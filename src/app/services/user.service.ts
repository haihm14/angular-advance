import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericResponse } from '../models/genericResponse';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAllUser() {
    return this.http.get<GenericResponse>(`${environment.apiUrl}/api/ManageUser/GET`);
  }

  createUser(data: User) {
    return this.http.post<GenericResponse>(`${environment.apiUrl}/api/ManageUser/Create`, data);
  }

  updateUser(updateData: User) {
    return this.http.put<GenericResponse>(`${environment.apiUrl}/api/ManageUser/Update`, updateData);
  }

  deleteUser(id: string) {
    return this.http.delete(`${environment.apiUrl}/api/ManageUser/Delete` + id);
  }

  getRoleOfUser() {
    const roles = localStorage.getItem("roles");
    if (roles != null) {
      let data = JSON.parse(roles);
      return data.role;
    }
    return "";
  }

  getUserInfor() {
    return this.http.get<any>(`${environment.apiUrl}/api/current-user`);
  }
}
