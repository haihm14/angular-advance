import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../services/authentication.service';
import { stringify } from 'querystring';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with basic auth credentials if available
        const token = this.authenticationService.getAccessToken();
        if (token != null && token.length != 0) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ` + token
                }
            });
        }

        return next.handle(request);
    }
}