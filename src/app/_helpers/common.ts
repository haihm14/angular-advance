export const enum ROLE {
    ADMIN = "admin",
    USER = "employee",
}

export const enum NotificationType {
    SUCCESS = "success",
    ERROR = "error",
}