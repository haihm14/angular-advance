import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user';

import { AuthenticationService } from '../services/authentication.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authenticationService.isAuthenticated()) {
            // const expectedRoles = route.data.expectedRoles as [];
            // const role  = JSON.parse(localStorage.getItem('role'));
            // if (
            //     !this.authenticationService.isAuthenticated() ||
            //     expectedRoles.filter(x => x == role).length == 0
            // ) {
            //     this.router.navigate(['/login']);
            //     return false;
            // }
            return true;
        }
        this.router.navigate(['/login'], { queryParams: { redirect: state.url }, replaceUrl: true });
        return false;
    }
}