import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './models/user';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { ROLE } from './_helpers/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isCollapsed = false;
  role: string = "";
  isLogin = false;
  user: User;
  userName: string;

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService) {

  }
  ngOnInit(): void {
    this.loadMenu();
  }

  loadMenu() {
    debugger;
    console.log('load menu', this.authenticationService.name);
    //   if (res) {
    //     localStorage.setItem('role', JSON.stringify(res.roles[0]));
    //     this.role = res.roles[0];
    //     debugger;
    //     if (this.role === ROLE.ADMIN) {
    //       this.menuOfUser = this.listMenu.filter(x => x.name != 'Login');
    //     } else if (this.role === ROLE.USER) {
    //       this.menuOfUser = this.listMenu.filter(x => x.name != "User Management" && x.name != "Login");
    //     } else {
    //       this.menuOfUser = this.listMenu.filter(x => x.name == 'Login');
    //     }
    //   }
    // });
    
  }

  SignOut() {
    localStorage.clear();
    this.authenticationService.signout();
  }
}
