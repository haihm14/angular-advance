import { Location } from '@angular/common';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { title } from 'process';
import {  } from 'rxjs';
interface BreadCrumb {
  title?: string;
  link?: string;
}

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumb: BreadCrumb[] = [];

  constructor(private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.router.events.pipe(
    ).subscribe((e: RouterEvent) => {
      this.updateBreadcrumb();
    });
  }

  updateBreadcrumb(){
    let slug = "./";
    let localbreadcrumb: BreadCrumb[] = [{ title: "Home", link: "./" }]
     const data = location.pathname.split('/')
      .map(e => {
        if (e !== "" || !this.breadcrumb.find(x => x.title === e)) {
          slug += "/" + e;
          localbreadcrumb.push({ title: e, link: slug })
        }
      })
      this.breadcrumb = localbreadcrumb;
  }
}
