export interface User {
    id?: string,
    code?: string,
    userName?: string,
    fullName?: string,
    email?: string,
    birthday?: Date,
    password?: string,
    confirmPassword?: string,
    phoneNumber?: string,
    lockoutEnabled?: boolean,
    authdata?: string,
    role?: string, 
}