export interface GenericResponse {
    code?: string,
    success?: string,
    content?: any,
    message?: string,
    totalRecords?: number,
}