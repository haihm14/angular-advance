import { Injectable } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  createNotification(type: string, title: string, contain: string): void {
    this.notification.create(
      type,
      title,
      contain
    );
  }

  constructor(private notification: NzNotificationService) {}
}
