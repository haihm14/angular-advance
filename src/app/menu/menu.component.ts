import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { ROLE } from '../_helpers/common';


interface IMenu {
  link?: string,
  name?: string,
}


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  listMenu: IMenu[] = [
    {
      link: "/login",
      name: "Login"
    },
    {
      link: "/welcome",
      name: "Welcome"
    },
    {
      link: "/leave",
      name: "Leave Management"
    },
    {
      link: "/user",
      name: "User Management"
    }
  ];
  menuOfUser: IMenu[];
  role: string = '';

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.role  = JSON.parse(localStorage.getItem('user'))?.profile?.roles;
    if (this.role === ROLE.ADMIN) {
      this.menuOfUser = this.listMenu.filter(x => x.name != 'Login');
    } else if (this.role === ROLE.USER) {
      this.menuOfUser = this.listMenu.filter(x => x.name != "User Management" && x.name != "Login");
    } else {
      this.menuOfUser = this.listMenu.filter(x => x.name == 'Login');
    }
  }

}
