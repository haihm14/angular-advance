import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Observer } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { NotificationType } from 'src/app/_helpers/common';


function comparePw(c: FormGroup): ValidationErrors | null {
  const { password, confirmPassword } = c.value;
  if (password !== confirmPassword) {
    return {
      pwnotmatch: true
    };
  }
  return null;
}

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  addUserForm: FormGroup;
  listUser: User[] = []

  constructor(private fb: FormBuilder,
    private userService: UserService,
    private notification: NotificationService,
  ) {
    this.userService.getAllUser().subscribe(res => {
      if (res.success) {
        this.listUser = res.content as User[];
      }
    })
  }

  ngOnInit(): void {
    this.addUserForm = this.fb.group({
      code: ['', [Validators.required], [this.codeAsyncValidator]],
      userName: ['', [Validators.email, Validators.required], [this.userNameAsyncValidator]],
      fullName: ['', Validators.required],
      email: ['',],
      birthday: [''],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [this.confirmValidator]],
      phoneNumber: []
    });
  }

  validateConfirmPassword(): void {
    //setTimeout(() => this.addUserForm.controls.confirm.updateValueAndValidity());
  }


  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.addUserForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  submitForm(): void {
    for (const key in this.addUserForm.controls) {
      this.addUserForm.controls[key].markAsDirty();
      this.addUserForm.controls[key].updateValueAndValidity();
    }
    if (this.addUserForm.pending || this.addUserForm.valid) {
      const data = this.addUserForm.value as User;
      data.email = data.userName;
      this.userService.createUser(data).subscribe(res => {
        if (res.success) {
          this.notification.createNotification(NotificationType.SUCCESS, "Create success", "");
        } else {
          this.notification.createNotification(NotificationType.ERROR, "Error", res.message);
        }
      })
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.addUserForm.reset();
    for (const key in this.addUserForm.controls) {
      this.addUserForm.controls[key].markAsPristine();
      this.addUserForm.controls[key].updateValueAndValidity();
    }
    //this.router.navigate(['user']);
  }

  codeAsyncValidator = (control: FormControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      setTimeout(() => {
        if (this.listUser.filter(x => x.code === control.value).length == 1) {
          // you have to return `{error: true}` to mark it as an error event
          observer.next({ error: true, duplicated: true });
        } else {
          observer.next(null);
        }
        observer.complete();
      }, 1000);
    });

  userNameAsyncValidator = (control: FormControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      setTimeout(() => {
        if (this.listUser.filter(x => x.userName === control.value).length == 1) {
          // you have to return `{error: true}` to mark it as an error event
          observer.next({ error: true, duplicated: true });
        } else {
          observer.next(null);
        }
        observer.complete();
      }, 1000);
    });

}
