import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { NotificationType } from 'src/app/_helpers/common';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  editCache: { [key: string]: { edit: boolean; data: User } } = {};
  listOfData: User[] = [];

  constructor(private router: Router,
    private notification: NotificationService,
    private userService: UserService,
  ) {
  }

  public ngOnInit(): void {
    this.getAllUser();
  }

  startEdit(id: string): void {
    this.editCache[id].edit = true;
  }

  cancelEdit(id: string): void {
    const index = this.listOfData.findIndex(item => item.id === id);
    this.editCache[id] = {
      data: { ...this.listOfData[index] },
      edit: false
    };
  }

  saveEdit(id: string): void {
    if (+id != undefined) {
      const index = this.listOfData.findIndex(item => item.id === id);
      Object.assign(this.listOfData[index], this.editCache[id].data);
      this.editCache[id].edit = false;
      this.userService.updateUser(this.editCache[id].data).subscribe(res => {
        console.log(res);
        if (res.success) {
          this.getAllUser();
          this.notification.createNotification(NotificationType.SUCCESS, "Update success", "");
        }
      })
    }

  }

  updateEditCache(): void {
    this.listOfData.forEach(item => {
      this.editCache[item.id] = {
        edit: false,
        data: { ...item }
      };
    });
  }

  CreateUser() {
    //this.notification.createNotification("success", "Title", "Created success!")
    this.router.navigate(['user/create-user']);
  }

  onChange(result: Date): void {
    console.log('onChange: ', result);
  }

  getAllUser() {
    this.userService.getAllUser().subscribe(res => {
      if (res.success) {
        this.listOfData = res.content as User[];
        // Custom DATA
        console.log(this.listOfData);
        const data: User[] = [];
        for (let i = 0; i < this.listOfData.length; i++) {
          data.push({
            id: this.listOfData[i].id,
            code: this.listOfData[i].code,
            userName: this.listOfData[i].userName,
            fullName: this.listOfData[i].fullName,
            email: this.listOfData[i].email,
            birthday: this.listOfData[i].birthday,
            phoneNumber: this.listOfData[i].phoneNumber,
            lockoutEnabled: this.listOfData[i].lockoutEnabled,
          });
        }
        this.listOfData = data;
        this.updateEditCache();
      }
    })
  }

}
