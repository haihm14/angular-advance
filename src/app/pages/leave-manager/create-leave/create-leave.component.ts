import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidationErrors } from '@angular/forms';
import getISOWeek from 'date-fns/getISOWeek';
import { Observable, Observer } from 'rxjs';
import { LogLeave } from 'src/app/models/Leave';
import { LogLeaveService } from 'src/app/services/log-leave.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { NotificationType } from 'src/app/_helpers/common';

@Component({
  selector: 'app-create-leave',
  templateUrl: './create-leave.component.html',
  styleUrls: ['./create-leave.component.scss'],
})
export class CreateLeaveComponent implements OnInit {
  logLeave: LogLeave;
  createLeaveForm!: FormGroup;

  constructor(private fb: FormBuilder,
    private logLeaveService: LogLeaveService,
    private notification: NotificationService,
  ) { }

  ngOnInit(): void {
    this.createLeaveForm = this.fb.group({
      id: [],
      typeID: [null, [Validators.required]],
      fromDate: [null],
      toDate: [null],
      dateRange: [[], Validators.required],
      reason: [null, [Validators.required]],
      comment: [''],
      isApproved: [false],
    });
  }

  onChange(result: Date[]): void {
    console.log('onChange: ', result);
  }

  getWeek(result: Date[]): void {
    console.log('week: ', result.map(getISOWeek));
  }

  submitForm(): void {
    for (const key in this.createLeaveForm.controls) {
      this.createLeaveForm.controls[key].markAsDirty();
      this.createLeaveForm.controls[key].updateValueAndValidity();
    }
    console.log(this.createLeaveForm.value);
    if (this.createLeaveForm.valid) {
      const data = this.createLeaveForm.value
      this.logLeave = {
        userID: data.userID,
        approver: data.approver,
        typeID: data.typeID,
        fromDate: data.dateRange[0],
        toDate: data.dateRange[1],
        reason: data.reason,
        comment: data.comment,
      }
      console.log('DATA', this.logLeave);
      this.logLeaveService.createLogLeave(this.logLeave).subscribe(res => {
        if (res) {
          this.notification.createNotification(NotificationType.SUCCESS, "Create success", "");
        }
      })
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.createLeaveForm.reset();
    for (const key in this.createLeaveForm.controls) {
      this.createLeaveForm.controls[key].markAsPristine();
      this.createLeaveForm.controls[key].updateValueAndValidity();
    }
  }
}
