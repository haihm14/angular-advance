import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateLeaveComponent } from './create-leave/create-leave.component';
import { LeaveListComponent } from './leave-list/leave-list.component';

const routes: Routes = [
  { path: '', component: LeaveListComponent },
  { path: 'create-leave', component: CreateLeaveComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeaveRoutingModule { }
