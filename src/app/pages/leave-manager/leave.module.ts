import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzTableModule } from 'ng-zorro-antd/table';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { LeaveRoutingModule } from './leave-routing.module';
import { CreateLeaveComponent } from './create-leave/create-leave.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { HttpClientModule } from '@angular/common/http';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { NzSelectModule } from 'ng-zorro-antd/select';


@NgModule({
  declarations: [LeaveListComponent, CreateLeaveComponent],
  imports: [
    CommonModule,
    NzTableModule,
    LeaveRoutingModule,
    IconsProviderModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NzInputModule,
    NzGridModule,
    NzFormModule,
    NzLayoutModule,
    NzMenuModule,
    NzDatePickerModule,
    NzButtonModule,
    NzSelectModule,
    
  ]
})
export class LeaveModule { }
