import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogLeave } from 'src/app/models/Leave';
import { LogLeaveService } from 'src/app/services/log-leave.service';
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { NotificationType, ROLE } from 'src/app/_helpers/common';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.scss']
})
export class LeaveListComponent implements OnInit {
  editCache: { [key: string]: { edit: boolean; data: LogLeave } } = {};
  listOfData: LogLeave[] = [];
  canApproved: boolean = false;
  role: string = "";

  constructor(private router: Router,
    private logLeaveService: LogLeaveService,
    private userService: UserService,
    private notification: NotificationService,
  ) { }

  ngOnInit(): void {
    this.role  = JSON.parse(localStorage.getItem('user')).profile.roles;
    if (this.role === ROLE.ADMIN) {
      this.canApproved = true;
    }
    this.getAllLogLeave();
  }

  startEdit(id: string): void {
    this.editCache[id].edit = true;
  }

  cancelEdit(id: string): void {
    const index = this.listOfData.findIndex(item => item.id === id);
    this.editCache[id] = {
      data: { ...this.listOfData[index] },
      edit: false
    };
  }

  saveEdit(id: string): void {
    if (+id != undefined) {
      const index = this.listOfData.findIndex(item => item.id === id);
      Object.assign(this.listOfData[index], this.editCache[id].data);
      this.logLeaveService.updateLogLeave(this.editCache[id].data).subscribe(res => {
        if (res) {
          this.getAllLogLeave();
          this.notification.createNotification(NotificationType.SUCCESS, "Update success", "");
        }
      })
    }
  }

  updateEditCache(): void {
    this.listOfData.forEach(item => {
      this.editCache[item.id] = {
        edit: false,
        data: { ...item }
      };
    });
  }

  CreateLeave() {
    this.router.navigate(['leave/create-leave']);
  }

  onChange(result: Date): void {
    console.log('onChange: ', result);
  }

  acceptLogLeave(id: string) {
    if (+id != undefined) {
      const index = this.listOfData.findIndex(item => item.id === id);
      this.listOfData[index].status = 1;
      Object.assign(this.listOfData[index], this.editCache[id].data);
      this.editCache[id].data.status = 1;
      this.logLeaveService.updateLogLeave(this.editCache[id].data).subscribe(res => {
        if (res) {
          this.getAllLogLeave();
          this.notification.createNotification(NotificationType.SUCCESS, "Approved success", "");
        }
      })
    }

  }

  getAllLogLeave() {
    this.logLeaveService.getAllLogLeave().subscribe(res => {
      if (res.success) {
        this.listOfData = res.content as LogLeave[];
        // Custom DATA
        console.log(this.listOfData);
        const data: LogLeave[] = [];
        for (let i = 0; i < this.listOfData.length; i++) {
          data.push({
            id: this.listOfData[i].id,
            userID: this.listOfData[i].userID,
            userFullName: this.listOfData[i].userFullName,
            approver: this.listOfData[i].approver,
            approverName: this.listOfData[i].approverName,
            typeID: this.listOfData[i].typeID,
            typeName: this.listOfData[i].typeName,
            fromDate: this.listOfData[i].fromDate,
            toDate: this.listOfData[i].toDate,
            reason: this.listOfData[i].reason,
            comment: this.listOfData[i].comment,
            status: this.listOfData[i].status ?? 0,
            statusName: this.listOfData[i].statusName,
            createDate: this.listOfData[i].createDate,
          });
        }
        this.listOfData = data;
        this.updateEditCache();
      }
    })
  }
}
