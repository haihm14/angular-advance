import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(private userService: UserService,
    private authenticationService: AuthenticationService,
    ) {
    console.log('init welcome');
    // this.authenticationService.getUser();
    // this.userService.getUserInfor().subscribe(res => {
    //   if (res) {
    //     localStorage.setItem('role', JSON.stringify(res.roles[0]));
    //   }
    // });
  }

  ngOnInit() {
    // this.authenticationService.getUser();
    // this.userService.getUserInfor().subscribe(res => {
    //   if (res) {
    //     localStorage.setItem('role', JSON.stringify(res.roles[0]));
    //   }
    // });
  }

}
