import { UserManagerSettings } from 'oidc-client';

export function getClientSettings(): UserManagerSettings {
    return {
        // authority: 'https://localhost:5001/',
        // client_id: 'spa',
        // redirect_uri: 'http://localhost:4200/auth-callback',
        // post_logout_redirect_uri: 'http://localhost:4200',
        // response_type: "code",
        // scope: "email openid profile roles",
        // filterProtocolClaims: true,
        // loadUserInfo: true,
        // automaticSilentRenew: true,
        authority: "http://hoangvh030490-001-site1.itempurl.com",
        client_id: 'angular_spa',
        redirect_uri: 'http://localhost:4200/auth-callback',
        post_logout_redirect_uri: 'http://localhost:4200',
        response_type: 'code',
        scope: 'email openid profile roles',
        filterProtocolClaims: true,
        loadUserInfo: true,
        automaticSilentRenew: true,
        silent_redirect_uri: 'http://localhost:4200/silent-refresh.html'

    };
}