import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserManager } from 'oidc-client';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { getClientSettings } from './getClientSettings';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  login() {
    //this.spinner.show();
    this.authenticationService.startAuthentication();
  }
  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.login();
  }

}
