import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthCallbackComponent } from './_helpers/auth-callback/auth-callback.component';
import { AuthGuard } from './_helpers/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'auth-callback', component: AuthCallbackComponent },
  {
    path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module')
      .then(m => m.WelcomeModule), canActivate: [AuthGuard], data: { expectedRoles: ['admin', 'employee'] }
  },
  {
    path: 'leave', loadChildren: () => import('./pages/leave-manager/leave.module')
      .then(m => m.LeaveModule), canActivate: [AuthGuard], data: { expectedRoles: ['admin', 'employee'] }
  },
  {
    path: 'user', loadChildren: () => import('./pages/user-manager/user.module')
      .then(m => m.UserModule), canActivate: [AuthGuard], data: { expectedRoles: ['admin'] }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
